<?xml version="1.0"?>
<info>
    <id>phonetrack</id>
    <name>PhoneTrack</name>
    <description>
# PhoneTrack Owncloud application
# PhoneTrack Nextcloud application

PhoneTrack is an app to get tracking information from mobile devices
and display them dynamically on a Leaflet map. Principle is simple :

* create a tracking session
* give the tracking URL\* to the mobile devices. Several methods :
    * With a web browser, on the session public logging page, check "Log my position in this session" (works better on Android than on IOS...)
    * [OsmAnd gpx recording plugin](https://osmand.net/features?id=trip-recording-plugin#Online_tracking) (Android) is able to log to a custom URL with GET method. IOS version does not include recording plugin. Tested and approved.
    * [GpsLogger](http://code.mendhak.com/gpslogger/#features) (Android) Very good ! Setup in : Options -> Logging details -> Log to custom URL
    * [Owntracks](http://owntracks.org/) (IOS/Android) Both version work. This app does not work without google services installed on Android. Quite funny to provide an app for those who want to keep control of their tracking information and force them to be tracked by google services...
    * [µlogger](https://f-droid.org/packages/net.fabiszewski.ulogger/) (Android) The best IMHO. Very light. Bufferize positions when device looses connectivity and sends everything when back online.
    * [Traccar](https://www.traccar.org/client/) (IOS/Android) Quite good, not very verbose. Also able to bufferize.
    * [OpenGTS](http://opengts.org/) which is more a standard than an app. I successfully used [GpsLogger](http://code.mendhak.com/gpslogger/#features) (OpenGTS mode) and [CelltrackGTS/Free](http://www.geotelematic.com/CelltracGTS/Free.html) (a few bugs with this one).
    * Does anyone know good and free ([as in "free speech"](https://www.gnu.org/philosophy/free-sw.en.html)) app which can log position to custom URL in background ? Create an issue if you do !
* Watch the session's devices positions in real time (or not) in PhoneTrack normal or public page

(\*) Don't forget to set the device name in the URL. Replace "yourname" with the desired device name. Setting the device name in logging app only works with Owntracks, Traccar and OpenGTS.

On PhoneTrack main page, while watching a session, you can :

* edit/add/delete points
* select some devices to make the automatic zoom work only with those devices
* toggle position history display (path lines)
* toggle devices last point date display
* rename a session
* share a session to a list of users (read-only)
* make a session public and share it via public link. If session is not public, positions are not visible in web logging page "publicWebLog".
* export sessions in GPX format (one subtrack by device). They are saved in Nextcloud files.
* export sessions in GPX format (one subtrack by device). They are saved in ownCloud files.

Public page works like main page except there is only one session displayed and there is no need to be logged in.

If you want to help to translate this app in your language, take the english=>french files in "l10n" directory as examples.

This app is tested with Owncloud 10 with Firefox 56+ and Chromium.
This app is tested with Nextcloud 12 with Firefox 56+ and Chromium.

This app is under development.

Link to Owncloud application website : https://marketplace.owncloud.com/apps/phonetrack

Link to Nextcloud application website : https://apps.nextcloud.com/apps/phonetrack

## Donation

I develop this app during my free time. You can make a donation to me on Paypal. [There is a donation link HERE](https://gitlab.com/eneiluj/phonetrack-oc#donation) (you don't need a paypal account)

## Install

See the [AdminDoc](https://gitlab.com/eneiluj/phonetrack-oc/wikis/admindoc) for more details.

Put phonetrack directory in the Nextcloud apps to install.
Put phonetrack directory in the Owncloud apps to install.
There are several ways to do that :

### Clone the git repository

```
cd /path/to/owncloud/apps
git clone https://gitlab.com/eneiluj/phonetrack-oc.git phonetrack
```

### Download from https://marketplace.owncloud.com
### Download from https://apps.nextcloud.com

Extract phonetrack archive you just downloaded from the website :
```
cd /path/to/owncloud/apps
cd /path/to/nextcloud/apps
tar xvf phonetrack-x.x.x.tar.gz
```

## Known issues

Any feedback will be appreciated.

    </description>
    <licence>AGPL</licence>
    <summary>Display phones positions in real time</summary>
    <author>Julien Veyssier</author>
    <version>0.0.5</version>
    <namespace>PhoneTrack</namespace>
    <documentation>
        <user>https://gitlab.com/eneiluj/phonetrack-oc/wikis/userdoc</user>
        <admin>https://gitlab.com/eneiluj/phonetrack-oc/wikis/admindoc</admin>
        <developer>https://gitlab.com/eneiluj/phonetrack-oc/wikis/devdoc</developer>
    </documentation>
    <category>tool</category>
    <category>multimedia</category>
    <website>https://gitlab.com/eneiluj/phonetrack-oc</website>
    <bugs>https://gitlab.com/eneiluj/phonetrack-oc/issues</bugs>
    <screenshot>https://gitlab.com/eneiluj/phonetrack-oc/uploads/58881f5d155a91aec3c0db9f051126e0/pt1.jpeg</screenshot>
    <screenshot>https://gitlab.com/eneiluj/phonetrack-oc/uploads/9ee540e94ca32fce8747f411bbc9149b/pt2.jpeg</screenshot>
    <screenshot>https://gitlab.com/eneiluj/phonetrack-oc/uploads/97b3e41c3fa49ddebee52c3d9de540c7/pt3.jpeg</screenshot>
    <dependencies>
        <database min-version="9.4">pgsql</database>
        <database>sqlite</database>
        <database min-version="5.5">mysql</database>
        <php min-version="5.6"/>
        <owncloud min-version="9.0" max-version="10.9" />
        <nextcloud min-version="9.0" max-version="12.9"/>
    </dependencies>
</info>
